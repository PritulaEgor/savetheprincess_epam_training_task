﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaveThePrincess.Map
{
    public enum Complexity
    {
        Easy,
        Medium,
        Hard,
        Guide
    }
}
