﻿using System;
using System.Collections.Generic;
using System.Text;
using SaveThePrincess.Units;

namespace SaveThePrincess.Map
{
    class UserMap
    {
        public string[,] userMap = new string[11, 11];

        public UserMap(Hero hero, Princess princess)
        {
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 11; j++)
                {

                    if (i == 0 || i == 10)
                    {
                        userMap[i, j] = " #";
                    }
                    else if( j == 0)
                    {
                        userMap[i, j] = "# ";
                    }
                    else if ( j == 10)
                    {
                        userMap[i, j] = " #";
                    }
                    else
                    {
                        userMap[i, j] = "  ";
                    }

                    if (i == princess.spawnY && j == princess.spawnX)
                    {
                        userMap[i, j] = princess.icon;
                    }
                }
            }
        }
        public bool AreTheHeroReachetTheGoal(Hero hero, Princess princess)
        {

            if (hero.currentX == princess.spawnX && hero.currentY == princess.spawnY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void RenderUserMap(Hero hero)
        {
            Console.Clear();
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 11; j++)
                {
                    
                    if (i == hero.currentY && j == hero.currentX)
                    {
                        Console.Write(hero.Icon);
                    }
                    else
                    {
                        Console.Write(userMap[i, j]);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
