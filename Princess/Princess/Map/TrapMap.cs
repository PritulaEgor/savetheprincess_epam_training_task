﻿using System;
using System.Collections.Generic;
using System.Text;
using SaveThePrincess.Units;

namespace SaveThePrincess.Map
{
    class TrapMap
    {
        public string[,] trapMap = new string[11, 11];

        public TrapMap(Complexity complexity, Trap trap)
        {
            int trapsCount;
            if (complexity == Complexity.Easy)
            {
                trapsCount = 10;
            }
            else if (complexity == Complexity.Medium)
            {
                trapsCount = 20;
            }
            else if ( complexity == Complexity.Hard)
            {
                trapsCount = 30;
            }
            else
            {
                trapsCount = 1;
            }

            Random rnd = new Random();
            int trapChance = 100 / trapsCount;
            int trapsInTheField = 0;
            while (true)
            {
                for (int i = 1; i < 10; i++)
                {
                    for (int j = 1; j < 10; j++)
                    {
                        if ((i == 1 && j == 1) || (i == 9 && j == 9))
                        {
                            trapMap[i, j] = "  ";
                        }
                        else if (rnd.Next(0, trapChance+1) == trapChance && complexity != Complexity.Guide && (trapMap[i, j] == "  "||trapMap[i,j] != trap.icon))
                        {
                            trapMap[i, j] = trap.icon;
                            trapsInTheField += 1;
                        }
                        else if(trapMap[i,j]!=trap.icon)
                        {
                            trapMap[i, j] = "  ";
                        }
                    }
                }
                if (trapsInTheField >= trapsCount||complexity==Complexity.Guide)
                {
                    break;
                }
            }
        }
        public void RenderTrapMap(Hero hero)
        {
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 11; j++)
                {

                    if (i == hero.currentY && j == hero.currentX)
                    {
                        Console.Write(hero.Icon);
                    }
                    else
                    {
                        Console.Write(trapMap[i, j]);
                    }
                }
                Console.WriteLine();
            }
        }
        public string CheckHeroPosition(Hero hero, Trap trap, UserMap map)
        {
            Random evasion = new Random();
            int evade = evasion.Next(0, hero.evasionChance + 1);
            if (trapMap[hero.currentY, hero.currentX] == "X" && hero.armour > 0 && evade != hero.evasionChance)
            {
                hero.armour -= trap.damage;
                map.userMap[hero.currentY, hero.currentX] = "X ";
                return "whoops, you got in a trap...\n" +
                    "Be carefull in future, you armour isn't endless";

            }
            else if (trapMap[hero.currentY, hero.currentX] == "X" && evade != hero.evasionChance)
            {
                hero.hitPoints -= trap.damage;
                map.userMap[hero.currentY, hero.currentX] = "X ";
                return "whoops, you got in a trap...\n" +
                   "Be carefull in future, you health isn't endless";
            }
            else if (trapMap[hero.currentY, hero.currentX] == "X" && evade == hero.evasionChance)
            {
                map.userMap[hero.currentY, hero.currentX] = "X ";
                return "You dodged a trap... You are lucky one\n" +
                    "but dont relax, princess isn't saved yet";
            }
            return "";
        }
    }
}
