﻿using System;
using System.Collections.Generic;
using System.Text;
using SaveThePrincess.Units;

namespace SaveThePrincess.Map
{
    class GuideMap
    {
        public void Guide()
        {
            Princess princess = new Princess();
            Hero hero = new Hero(100, 0, 10);
            Trap trap = new Trap(0);
            UserMap userMap = new UserMap(hero, princess);
            TrapMap trapMap = new TrapMap(Complexity.Guide, trap);
            int moves = 0;
            string endOfGuide = "";
            bool secondMessage = false;
            string message = "\tHello brave hero, to save the Princes but first you need to reach her location\n" +
                "\tPress \"WASD\" buttons or \"numPad8, numPad4, numPad2, numPad6\" or \"Arrow buttons\" to move \n" +
                $"\tMove {10 - moves} times to continue...";
            bool heroWasInATrap = false;
            while (true)
            {
                int x = hero.currentX;
                int y = hero.currentY;
                int evasionChance = 100 / hero.evasionChance;
                userMap.RenderUserMap(hero);
                if (moves == 10 && secondMessage == false)
                {
                    trapMap.trapMap[5, 5] = "X";
                    userMap.userMap[5, 5] = "X ";
                    message = "\t Good job brave hero\n" +
                        "\tnow i want to show you how traps work\n" +
                        "\tLook at your map, do you see that? \"X\" on your map\n" +
                        "\tit is your worst enemy - trap, it can kill you\n" +
                        "\tbut in the game, they will appear on the map after you get into them\n" +
                        "\tBut now it's a training area, and traps are fake\n" +
                        "\tcmon, aproach it, it doesn't bite";
                    secondMessage = true;
                    userMap.RenderUserMap(hero);
                }
                else if (secondMessage == false)
                {
                    message = "\tHello brave hero, to save the Princes but first you need to reach her location\n" +
                                   "\tPress \"WASD\" buttons or \"numPad8, numPad4, numPad2, numPad6\" or \"Arrow buttons\" to move \n" +
                                   $"\tMove {10 - moves} times to continue...";
                }
                if (endOfGuide != "")
                {
                    heroWasInATrap = true;
                    message = "\tDid you saw that? Its not hurts, doesnt it? But it will not always be so\n" +
                        "\tnow you can end this guide. Cmon. Go and save your sweet pincess, brave hero";
                }
                Console.WriteLine("HP {0}\n" +
                    "Armour 0 \n" +
                    "Evasion chance {1}%\n" +
                    "{2}", hero.hitPoints, evasionChance, message);

                ConsoleKeyInfo button = Console.ReadKey();
                if (button.Key == ConsoleKey.W || button.Key == ConsoleKey.NumPad8 || button.Key == ConsoleKey.UpArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Up);
                    moves += 1;
                }
                else if (button.Key == ConsoleKey.A || button.Key == ConsoleKey.NumPad4 || button.Key == ConsoleKey.LeftArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Left);
                    moves += 1;

                }
                else if (button.Key == ConsoleKey.S || button.Key == ConsoleKey.NumPad2 || button.Key == ConsoleKey.DownArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Down);
                    moves += 1;

                }
                else if (button.Key == ConsoleKey.D || button.Key == ConsoleKey.NumPad6 || button.Key == ConsoleKey.RightArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Right);
                    moves += 1;

                }

                hero.currentX = x;
                hero.currentY = y;

                endOfGuide = trapMap.CheckHeroPosition(hero, trap, userMap);
                if (userMap.AreTheHeroReachetTheGoal(hero, princess) && heroWasInATrap)
                {
                    Console.Clear();
                    Console.WriteLine("\t This is it! Your goal! But... its training area and princess, like a traps, are not real\n" +
                        "\tits just dummy. Real princes is in a dangerous dangeon now. But you are ready to save her\n" +
                        "\tGo and save your lover brave hero, farewell and may the force be with you\n" +
                        "\tPress any button to continue...");
                    Console.ReadKey();
                    break;

                }
                if (userMap.AreTheHeroReachetTheGoal(hero, princess) && !heroWasInATrap)
                {
                    Console.Clear();
                    Console.WriteLine("\t Hey dude! I am a joke for you? I said:\"Test the trap\"\n" +
                        "\tAm i trying to help you save your princess, but you dont want listen to me\n" +
                        "\tOK, i'm going to get you back on the field. But this time listen to my advice! Got it?\n" +
                        "\tPress any button to continue...");
                    Console.ReadKey();

                }

            }


        }
    }
}
