﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaveThePrincess.Units
{
    abstract class Unit
    {
        public int hitPoints { get; set; }

        public int damage { get; set; }

        public int spawnX { get; set; }

        public int spawnY { get; set; }

        public void Move(ref int x, ref int y, MovingDirections direction)
        {
            if (direction == MovingDirections.Right && x < 9)
            {
                x += 1;
            }
            else if (direction == MovingDirections.Left && x > 1)
            {
                x -= 1;
            }
            else if (direction == MovingDirections.Down && y < 9)
            {
                y += 1;
            }
            else if (direction == MovingDirections.Up && y > 1)
            {
                y -= 1;
            }
        }



    }
}
