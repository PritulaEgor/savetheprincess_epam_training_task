﻿using System;
using System.Collections.Generic;
using System.Text;
using SaveThePrincess.Map;

namespace SaveThePrincess.Units
{
    class Princess : Unit
    {
        public readonly string icon = "P ";

        public Princess()
        {
            spawnX = 9;
            spawnY = 9;
        }
    }
}
