﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaveThePrincess.Units
{
   public enum MovingDirections
    {
        Right,
        Left,
        Up,
        Down
    }
}
