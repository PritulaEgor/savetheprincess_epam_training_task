﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaveThePrincess.Units
{
    class Hero : Unit
    {
        public int evasionChance { get; set; }

        public int armour { get; set; }

        public readonly string Icon = "H ";

        public int currentX { get; set; }

        public int currentY { get; set; }

        public Hero (int evasionChance, int armour, int hitPoints)
        {
            this.evasionChance = evasionChance;
            this.armour = armour;
            this.hitPoints = hitPoints;
            currentX = 1;
            currentY = 1;
        }


    }
}
