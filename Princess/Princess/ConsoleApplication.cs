﻿using System;
using System.Collections.Generic;
using System.Text;
using SaveThePrincess.Units;
using SaveThePrincess.Map;

namespace SaveThePrincess
{
    class ConsoleApplication
    {
        public void MainMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\tWelcome to the game\n" +
                    "\tDANGEROUS DANGEON\n" +
                    "\t1. Start the game\n" +
                    "\t2. Guide\n" +
                    "\t3. Exit");
                ConsoleKeyInfo menueButton = Console.ReadKey();
                if (menueButton.KeyChar == '1' || menueButton.Key == ConsoleKey.NumPad1)
                {
                    Random rand = new Random();
                    string message = "";
                    int hitPoints = 10;
                    int evasion = 2;
                    int armour = 10;
                    Trap trap = new Trap (rand.Next(1, 10));
                    Complexity complexity;
                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine("\tBy the first choose the complexity of the game\n" +
                            "\t1. Easy (10 traps)\n" +
                            "\t2. Medium (20 traps)\n" +
                            "\t3. Hard (30 traps)");
                        ConsoleKeyInfo complexityButton = Console.ReadKey();
                        if (complexityButton.KeyChar == '1' || complexityButton.Key == ConsoleKey.NumPad1)
                        {
                            complexity = Complexity.Easy;
                            break;
                        }
                        else if (complexityButton.KeyChar == '2' || complexityButton.Key == ConsoleKey.NumPad2)
                        {
                            complexity = Complexity.Medium;
                            break;
                        }
                        else if (complexityButton.KeyChar == '3' || complexityButton.Key == ConsoleKey.NumPad3)
                        {
                            complexity = Complexity.Hard;
                            break;
                        }
                        
                    }
                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine("\tNow choose your hero\n" +
                            "\t1. Default hero - 20 HP, 0 Armour, 20% Evasion chance\n" +
                            "\t2. Dodger - 10 HP, 0 Armour, 50% Evasion chance\n" +
                            "\t3. Heavy knight - 30 HP, 10 Armour, 5% Evasion chance");
                        ConsoleKeyInfo heroButton = Console.ReadKey();
                        if (heroButton.KeyChar == '1' || heroButton.Key == ConsoleKey.NumPad1)
                        {
                            hitPoints = 20;
                            armour = 0;
                            evasion = 5;

                            break;
                        }
                        else if (heroButton.KeyChar == '2' || heroButton.Key == ConsoleKey.NumPad2)
                        {
                            hitPoints = 10;
                            armour = 0;
                            evasion = 2;

                            break;
                        }
                        else if (heroButton.KeyChar == '3' || heroButton.Key == ConsoleKey.NumPad3)
                        {
                            hitPoints = 30;
                            armour = 10;
                            evasion = 20;

                            break;
                        }

                    }
                    Hero hero = new Hero(evasion, armour, hitPoints);
                    Princess princess = new Princess();
                    UserMap userMap = new UserMap(hero, princess);
                    TrapMap trapMap = new TrapMap(complexity, trap);
                    Game(hero, trapMap, userMap, princess, trap, hitPoints, complexity, message);

                }
                else if (menueButton.KeyChar == '2' || menueButton.Key == ConsoleKey.NumPad2)
                {
                    Console.Clear();
                    Console.WriteLine("\tIn this game your main and only aim - save the princess\n" +
                        "\t Princess marked on a map an icon \"P\"\n" +
                        "\t Your hero \"H\"\n" +
                        "\t Under the map, you can see your HP, Armour, Evasion chance and\n" +
                        "\t message when you got in a trap\n" +
                        "\t You must reach the princess location before traps kills you\n" +
                        "\t After triggering, traps do not disappear and you can get in them again\n" +
                        "\t After triggering, traps are marked on the map an icon \"X\"\n" +
                        "\t Press \"WASD\" buttons or \"numPad8, numPad4, numPad2, numPad6\" or \"Arrow buttons\" to move \n" +
                        "Press any button to continue...");
                    Console.ReadKey();
                    GuideMap guide = new GuideMap();
                    guide.Guide();
                    
                }
                else if (menueButton.KeyChar == '3' || menueButton.Key == ConsoleKey.NumPad3)
                {
                    break;
                }
            }
        }
        public void Game(Hero hero, TrapMap trapMap, UserMap userMap, Princess princess, Trap trap, int hitPoints, Complexity complexity, string message)
        {
            while (true)
            {
                int x = hero.currentX;
                int y = hero.currentY;
                int evasionChance = 100 / hero.evasionChance;
                userMap.RenderUserMap(hero);
                if (hero.armour > 0)
                {
                    Console.WriteLine("HP {0}\n" +
                        "Armour {1} \n" +
                        "Evasion chance {2}%\n" +
                        "{3}", hero.hitPoints, hero.armour, evasionChance, message);
                }
                else
                {
                    Console.WriteLine("HP {0}\n" +
                        "Armour 0 \n" +
                        "Evasion chance {1}%\n" +
                        "{2}", hero.hitPoints, evasionChance, message);
                }
                ConsoleKeyInfo button = Console.ReadKey();
                if (button.Key == ConsoleKey.W || button.Key == ConsoleKey.NumPad8||button.Key == ConsoleKey.UpArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Up);

                }
                else if (button.Key == ConsoleKey.A || button.Key == ConsoleKey.NumPad4 || button.Key == ConsoleKey.LeftArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Left);

                }
                else if (button.Key == ConsoleKey.S || button.Key == ConsoleKey.NumPad2 || button.Key == ConsoleKey.DownArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Down);

                }
                else if (button.Key == ConsoleKey.D || button.Key == ConsoleKey.NumPad6 || button.Key == ConsoleKey.RightArrow)
                {
                    hero.Move(ref x, ref y, MovingDirections.Right);

                }

                hero.currentX = x;
                hero.currentY = y;

                message = trapMap.CheckHeroPosition(hero, trap, userMap);
                if (userMap.AreTheHeroReachetTheGoal(hero, princess) || hero.hitPoints <= 0)
                {
                    if (Retry())
                    {
                        Random rnd = new Random();
                        hero.currentX = hero.currentY = 1;
                        hero.hitPoints = hitPoints;
                        trap = new Trap(rnd.Next(0,10));
                        userMap = new UserMap(hero, princess);
                        trapMap = new TrapMap(complexity, trap);
                    }
                    else
                    {
                        break;
                    }

                }
                bool Retry()
                {
                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine("The end\n" +
                            "Start new game?\n" +
                            "1. Yes\n" +
                            "2. No");
                        ConsoleKeyInfo endButton = Console.ReadKey();
                        if (endButton.KeyChar == '1' || endButton.Key == ConsoleKey.NumPad1)
                        {
                            return true;
                        }
                        else if (endButton.KeyChar == '2' || endButton.Key == ConsoleKey.NumPad2)
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }
}
