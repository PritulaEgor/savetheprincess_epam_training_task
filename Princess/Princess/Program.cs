﻿using System;
using SaveThePrincess.Map;
using SaveThePrincess.Units;

namespace SaveThePrincess
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleApplication console = new ConsoleApplication();

            console.MainMenu();

        }
        
    }
}
